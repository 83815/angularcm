import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { NewBookComponent } from './new-book/new-book.component';

const routes: Routes = [
  { path: 'book/view/:id', component: BookComponent},
  { path: '', component:BookListComponent},
  { path: 'book/edit/:id', component: BookEditComponent},
  { path: 'book/add', component: NewBookComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
