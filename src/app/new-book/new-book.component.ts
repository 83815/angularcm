import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../book';
import { Location } from '@angular/common';

@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.css']
})
export class NewBookComponent implements OnInit {

  book: Book;

  constructor(private bookService: BookService, private location: Location) { 
    this.book = new Book();
  }

  ngOnInit() {
  }

  save(book: Book) {
    this.bookService.addBook(book).subscribe(result => this.location.back());
  }

}
