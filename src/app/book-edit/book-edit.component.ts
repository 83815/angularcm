import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookService } from '../book.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  book: Book;

  constructor(private bookService: BookService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    const id = (Number)(this.route.snapshot.paramMap.get('id'));
    this.selectBook(id);
  }

  selectBook(selectedBookId: number){
    this.bookService.getBook(selectedBookId)
    .subscribe(bookFromService => this.book = bookFromService);
  }

  save(book: Book) {
    this.bookService.updateBook(book);
    //this.location.back();
  }
}
