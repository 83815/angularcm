import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookService } from '../book.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  selectedBook: Book;
  books: Book[];

  constructor(private bookService: BookService, private router: Router) { }

  ngOnInit() {

    this.bookService.getBooks().subscribe(booksFromService => this.books = booksFromService);
  }

  delete(bookId: number) {
    this.bookService.deleteBook(bookId);
  }

  goToNewBook() {
    this.router.navigateByUrl('book/add');
  }
}
