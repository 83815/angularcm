import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../book';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  //@Input()
  //title: string;
  //books: Book[];
  book: Book;

  constructor(private bookService: BookService, private route: ActivatedRoute, private location: Location) { }
  //constructor() {}

  ngOnInit() {
    const id = (Number)(this.route.snapshot.paramMap.get('id'));
    this.selectBook(id);
    //this.bookService.getBooks().subscribe(booksFromService => this.books = booksFromService);
  }

  //sayTitle() {
  //  alert(this.title)
  //}

  selectBook(selectedBookId: number){
    this.bookService.getBook(selectedBookId)
    .subscribe(bookFromService => this.book = bookFromService);
  }

  save(book: Book) {
    this.bookService.updateBook(book);
  }

  //delete(bookId: number) {
  //  this.bookService.deleteBook(bookId);
  //}

  goBack() {
    this.location.back();
  }
}
