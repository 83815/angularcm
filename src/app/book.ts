export class Book {
    public Id: number;
    public AuthorId: number;
    public Title: string;
    public PublishYear: number;
}
