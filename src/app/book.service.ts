import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from './book';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BookService {

  //constructor() { }

  private bookApiURL = 'http://localhost:1000/api/books'

  constructor(
    private http: HttpClient) { }

  public getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.bookApiURL);
  }

  public getBook(id: number) {
    let url = `${this.bookApiURL}/${id}`;
    return this.http.get<Book>(url);
  }

  public updateBook(book: Book) {
    let url = `${this.bookApiURL}/${book.Id}`;
    this.http.put<Book>(url, book, httpOptions)
    .subscribe(result => alert("Update Done!"));
  }

  public addBook(book: Book) {
    return this.http.post<Book>(this.bookApiURL, book, httpOptions)
    //.subscribe(result => alert("Book Added!"));
  }

  public deleteBook(id: number) {
    let url = `${this.bookApiURL}/${id}`;
    this.http.delete(url)
    .subscribe(result => alert("Book Deleted!"));
  }
}
